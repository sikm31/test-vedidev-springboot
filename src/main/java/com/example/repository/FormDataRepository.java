package com.example.repository;

import com.example.model.FormDataModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface FormDataRepository extends CrudRepository<FormDataModel, Long> {



	List<FormDataModel> getFromDataTS(Long ts);
	
	@Query("SELECT formid FROM FormDataModel WHERE formid<>'' GROUP BY formid ORDER BY COUNT(formid) DESC")
	List<String> getFromDataTopFive();


}
