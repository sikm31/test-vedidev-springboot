package com.example.controller;


import com.example.dao.UploadFileDTO;
import com.example.service.FormDataService;
import com.example.validator.FileValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

@Controller
public class FileUploadController {

	private static final Logger logger = LoggerFactory.getLogger(FileUploadController.class);

	private final FormDataService service;

	private final FileValidator fileValidator;

	@Autowired
	public FileUploadController(FormDataService service, FileValidator fileValidator) {
		this.service = service;
		this.fileValidator = fileValidator;
	}


	@RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
	public String getHomePage(ModelMap model) {
		UploadFileDTO fileModel = new UploadFileDTO();
		model.addAttribute("fileUpload", fileModel);
		return "home";
	}

	@RequestMapping(value = {"/", "/home"}, method = RequestMethod.POST)
	public String singleFileUpload(@Valid UploadFileDTO uploadFileDTO, BindingResult result, ModelMap model) throws IOException {

		fileValidator.validate(uploadFileDTO, result);
		if (result.hasErrors()) {
			logger.info("validation errors");
			model.addAttribute("fileUpload", uploadFileDTO);
			return "home";
		} else {
			MultipartFile multipartFile = uploadFileDTO.getFile();

			FileCopyUtils.copy(uploadFileDTO.getFile().getBytes(),
					new File(Objects.requireNonNull(uploadFileDTO.getFile().getOriginalFilename())));

			String fileName = multipartFile.getOriginalFilename();
			service.addFormData(fileName);
			model.addAttribute("fileName", fileName);
			return "success";
		}
	}

	@RequestMapping(value = "/lastHours", method = RequestMethod.GET)
	public String getLastHour(ModelMap model) {
		model.addAttribute("listDataHour", service.getFromDataTSLastHour());
		return "lastHours";
	}

	@RequestMapping(value = "/mostUsedForms", method = RequestMethod.GET)
	public String getMostUsedForms(ModelMap model) {
		model.addAttribute("mostUsedForms", service.getFromDataTopFive());
		return "mostUsedForms";

	}
}