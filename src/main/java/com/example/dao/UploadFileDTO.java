package com.example.dao;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;


@Getter
@Setter
public class UploadFileDTO {

	private MultipartFile file;
}
