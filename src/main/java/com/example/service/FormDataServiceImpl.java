package com.example.service;

import com.example.model.FormDataModel;
import com.example.repository.FormDataRepository;
import com.example.util.ConvertDtoFormData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Service
public class FormDataServiceImpl implements FormDataService {

	private final FileConvertService fileConvertService;

	private final FormDataRepository formDataRepository;


	@Autowired
	public FormDataServiceImpl(FileConvertService fileConvertService, FormDataRepository formDataRepository) {
		this.fileConvertService = fileConvertService;
		this.formDataRepository = formDataRepository;
	}


	@Override
	public void addFormData(String fileName) {
		formDataRepository.saveAll(ConvertDtoFormData.getConvertFormData(fileConvertService.convertUploadFile(fileName)));
	}

	@Override
	public List<FormDataModel> getFromDataTSLastHour() {
		Instant instant = Instant.now();
		Instant instantHourEarlier = instant.minus(1, ChronoUnit.HOURS);
		return formDataRepository.getFromDataTS(instantHourEarlier.getEpochSecond());//dao.getFromDataTS(instantHourEarlier.getEpochSecond());
	}

	@Override
	public List<String> getFromDataTopFive() {
		return formDataRepository.getFromDataTopFive();//dao.getFromDataTopFive();
	}


}
